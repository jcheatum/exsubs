# ExSubs
Extracts subtitles from video files to separate subtitle files using ffmpeg and names the output files so that they're compatible with Jellyfin naming conventions.

## Usage
```sh
$ exsubs <input files>
```
